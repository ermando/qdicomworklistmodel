QT -= gui

TEMPLATE = lib
DEFINES += QDICOMWORKLISTMODEL_LIBRARY

CONFIG += c++11
CONFIG(debug, debug|release){
    conf_dir="debug"
    TARGET = QDicomWorklistModeld
}
else{
    conf_dir="release"
    TARGET = QDicomWorklistModel
}
DLLDESTDIR = $PWD/../../bin
DESTDIR = $PWD/../../lib

SOURCES += \
    qdicomworklistmodel.cpp

HEADERS += \
    QDicomWorklistModel_global.h \
    qdicomworklistmodel.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

include("../dcmtk_3_6_6/dcmtk.pri")
