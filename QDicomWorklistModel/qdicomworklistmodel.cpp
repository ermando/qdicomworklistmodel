#include "qdicomworklistmodel.h"

#include <winsock.h>
#include "dcmtk/ofstd/ofstdinc.h"
#include "dcmtk/config/osconfig.h"      /* make sure OS specific configuration is included first */
#include "dcmtk/dcmnet/dfindscu.h"

class WorklistCallback:public DcmFindSCUCallback
{
public:
    WorklistCallback(QDicomWorklistModel* worklistClient){
        m_worklistClient=worklistClient;
    }
    virtual void callback(T_DIMSE_C_FindRQ *request,int &responseCount,T_DIMSE_C_FindRSP *rsp,DcmDataset *responseIdentifiers) override{
        m_worklistClient->callbackDataReady(request,responseCount,rsp,responseIdentifiers);
    }
private:
    QDicomWorklistModel* m_worklistClient;
};

QDicomWorklistModel::QDicomWorklistModel(QObject *parent)
    :QAbstractTableModel(parent)
{
    //TODO: use dictionary to build roles
    //DcmDataDictionary dic(true,false);
    //const DcmDictEntry *entry=dic.findEntry(DcmTagKey(0x10,0x10),nullptr);
    //qDebug()<<entry->getStandardVersion();
    //qDebug()<<entry->getTagName();
    m_roleNames[AccessionNumberRole]=QByteArrayLiteral("accessionNumber");
    m_roleNames[PatientNameRole]=QByteArrayLiteral("patientName");
    m_roleNames[PatientIdRole]=QByteArrayLiteral("patientId");
    m_roleNames[PatientBirthDateRole]=QByteArrayLiteral("patientBirthDate");
    m_roleNames[PatientSexRole]=QByteArrayLiteral("patientSex");
    m_roleNames[MedicalAlertsRole]=QByteArrayLiteral("medicalAlerts");
    m_roleNames[AllergiesRole]=QByteArrayLiteral("allergies");
    m_roleNames[StudyInstanceUIDRole]=QByteArrayLiteral("studyInstanceUID");
    m_roleNames[RequestingPhysicianRole]=QByteArrayLiteral("requestingPhysician");
    m_roleNames[RequestedProcedureDescriptionRole]=QByteArrayLiteral("requestedProcedureDescription");
    m_roleNames[ModalityRole]=QByteArrayLiteral("modality");
    m_roleNames[ScheduledStationAETitleRole]=QByteArrayLiteral("scheduledStationAETitle");
    m_roleNames[ScheduledProcedureStepStartDateTimeRole]=QByteArrayLiteral("scheduledProcedureStepStartDateTime");
    m_roleNames[ScheduledPerformingPhysicianNameRole]=QByteArrayLiteral("scheduledPerformingPhysicianName");
    m_roleNames[ModalityRole]="modality";
    m_roleNames[ScheduledStationAETitleRole]="scheduledStationAETitle";
    m_roleNames[ScheduledProcedureStepStartDateTimeRole]="scheduledProcedureStepStartDateTime";
    m_roleNames[ScheduledPerformingPhysicianNameRole]="scheduledPerformingPhysicianName";
}

QDicomWorklistModel::~QDicomWorklistModel()
{
}

int QDicomWorklistModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_data.count();
}

int QDicomWorklistModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return LastCol;
}

QVariant QDicomWorklistModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role==Qt::DisplayRole){
        if(orientation==Qt::Horizontal){
            //use rolenames to display header data
            return m_roleNames[Qt::UserRole+section+1];
        }
    }
    return QVariant();
}

QVariant QDicomWorklistModel::data(const QModelIndex &idx, int role) const
{
    int col=idx.column();

    if(role>Qt::UserRole){
        col=role-Qt::UserRole-1;
        role=Qt::DisplayRole;
    }

    switch(role){
    case Qt::DisplayRole:
        switch(col){
        case AccessionNumberCol:
            return m_data[idx.row()].accessionNumber;
        case PatientNameCol:
            return m_data[idx.row()].patientName;
        case PatientIdCol:
            return m_data[idx.row()].patientId;
        case PatientBirthDateCol:
            return m_data[idx.row()].patientBirthDate;
        case PatientSexCol:
            return m_data[idx.row()].patientSex;
        case MedicalAlertsCol:
            return m_data[idx.row()].medicalAlerts;
        case AllergiesRoleCol:
            return m_data[idx.row()].allergies;
        case StudyInstanceUIDCol:
            return m_data[idx.row()].studyInstanceUID;
        case RequestingPhysicianCol:
            return m_data[idx.row()].requestingPhysician;
        case RequestedProcedureDescriptionCol:
            return m_data[idx.row()].requestedProcedureDescription;
        case ModalityCol:
            return m_data[idx.row()].modality;
        case ScheduledStationAETitleCol:
            return m_data[idx.row()].scheduledStationAETitle;
        case ScheduledProcedureStepStartDateTimeCol:
            return m_data[idx.row()].scheduledProcedureStepStartDateTime;
        case ScheduledPerformingPhysicianNameCol:
            return m_data[idx.row()].scheduledPerformingPhysicianName;
        }
    }
    return QVariant();
}

QHash<int, QByteArray> QDicomWorklistModel::roleNames() const
{
    return m_roleNames;
}

void QDicomWorklistModel::setPeer(QString peer)
{
    if(m_peer!=peer){
        m_peer=peer;
        emit peerChanged();
    }
}

void QDicomWorklistModel::setPort(unsigned int port)
{
    if(m_port!=port){
        m_port=port;
        emit portChanged();
    }
}

void QDicomWorklistModel::setCallingAet(QString callingAet)
{
    if(m_callingAet!=callingAet){
        m_callingAet=callingAet;
        emit callingAetChanged();
    }
}

void QDicomWorklistModel::setCalledAet(QString calledAet)
{
    if(m_calledAet!=calledAet){
        m_calledAet=calledAet;
        emit calledAetChanged();
    }
}

bool QDicomWorklistModel::query()
{
    m_bufferData.clear();
    OFCondition cond;
    DcmFindSCU findscu;
    cond = findscu.initializeNetwork(30);//timeout
    if (cond.bad()) {
        return false;
    }
    OFList<OFString>      overrideKeys;
    WorklistCallback callback(this);
    overrideKeys.push_back("(0008,0050)");//accessionNumber
    overrideKeys.push_back("(0010,0010)");//patientName;
    overrideKeys.push_back("(0010,0020)");//patientId
    overrideKeys.push_back("(0010,0030)");//patientBirthDate;
    overrideKeys.push_back("(0010,0040)");//patientSex
    overrideKeys.push_back("(0010,2000)");//medicalAlerts
    overrideKeys.push_back("(0010,2110)");//"allergiesRole"
    overrideKeys.push_back("(0020,000d)");//studyInstanceUID
    overrideKeys.push_back("(0032,1032)");//requestingPhysician"
    overrideKeys.push_back("(0032,1060)");//requestedProcedureDescription

    //ScheduledProcedureStepSequence
    overrideKeys.push_back("(0040,0100)[0].(0008,0060)");//Modality
    overrideKeys.push_back("(0040,0100)[0].(0040,0001)");//ScheduledStationAETitle
    overrideKeys.push_back("(0040,0100)[0].(0040,0002)");//ScheduledProcedureStepStartDate
    overrideKeys.push_back("(0040,0100)[0].(0040,0003)");//ScheduledProcedureStepStartTime
    overrideKeys.push_back("(0040,0100)[0].(0040,0006)");//ScheduledPerformingPhysicianName
    cond=findscu.performQuery(
                m_peer.toLatin1(),
                m_port,
                m_callingAet.toLatin1(),
                m_calledAet.toLatin1(),
                UID_FINDModalityWorklistInformationModel,
                EXS_Unknown,
                DIMSE_BLOCKING,
                60,
                ASC_DEFAULTMAXPDU,
                false,
                false,
                1,
                FEM_none,
                false,
                &overrideKeys,
                &callback
                );

    updateData();
    return cond.good();
}

void QDicomWorklistModel::updateData()
{
    int row;
    int dataRow;
    int nRows;
    //remove from data if not in bufferData
    for(row=m_data.count()-1;row>=0;row--){
        if(!m_bufferDataUids.contains(m_data[row].studyInstanceUID)){
            dataRow=0;
            //search studyInstanceUID row in data
            for(const auto &item:m_data){
                if (item.studyInstanceUID==m_data[row].studyInstanceUID){
                    break;
                }
                dataRow++;
            }
            beginRemoveRows(QModelIndex(),dataRow,dataRow);
            m_dataUids.remove(m_data[dataRow].studyInstanceUID);
            m_data.removeAt(dataRow);
            endRemoveRows();
        }
    }
    //add items to data
    nRows=m_bufferData.count();
    for(row=0;row<nRows;row++){
        if(!m_dataUids.contains(m_bufferData[row].studyInstanceUID)){
            beginInsertRows(QModelIndex(),m_data.count(),m_data.count());
            m_data.append(m_bufferData[row]);
            m_dataUids <<m_bufferData[row].studyInstanceUID;
            endInsertRows();
        }
    }
    m_bufferData.clear();
    m_bufferDataUids.clear();
}

void QDicomWorklistModel::callbackDataReady(T_DIMSE_C_FindRQ *request, int &responseCount, T_DIMSE_C_FindRSP *rsp, DcmDataset *responseIdentifiers)
{
    Q_UNUSED(request)
    Q_UNUSED(responseCount)
    Q_UNUSED(rsp)
    WorklistItem item;

    QDateTime dateTime;
    OFString ofStr;

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x08,0x50),ofStr);
    item.accessionNumber=ofStr.c_str();

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x10,0x10),ofStr);
    item.patientName=ofStr.c_str();

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x10,0x20),ofStr);
    item.patientId=ofStr.c_str();

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x10,0x30),ofStr);
    item.patientBirthDate=QDate::fromString(ofStr.c_str(),QString("yyyyMMdd"));

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x10,0x40),ofStr);
    item.patientSex=(ofStr.size()>0)?ofStr.c_str()[0]:' ';

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0010,0x2000),ofStr);//medicalAlerts
    item.medicalAlerts=ofStr.c_str();

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0010,0x2110),ofStr);//"allergiesRole"
    item.allergies=ofStr.c_str();

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0020,0x000d),ofStr);//studyInstanceUID
    item.studyInstanceUID=ofStr.c_str();

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0032,0x1032),ofStr);//requestingPhysician"
    item.requestingPhysician=ofStr.c_str();

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0032,0x1060),ofStr);//requestedProcedureDescription
    item.requestedProcedureDescription=ofStr.c_str();

    //ScheduledProcedureStepSequence
    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0008,0x0060),ofStr,0,true);//Modality
    item.modality=ofStr.c_str();

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0040,0x0001),ofStr,0,true);//ScheduledStationAETitle
    item.scheduledStationAETitle=ofStr.c_str();

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0040,0x0002),ofStr,0,true);//ScheduledProcedureStepStartDate
    dateTime.setDate(QDate::fromString(ofStr.c_str(),QString("yyyyMMdd")));
    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0040,0x0003),ofStr,0,true);//ScheduledProcedureStepStartTime
    dateTime.setTime(QTime::fromString(ofStr.c_str(),QString("hhmmss")));
    item.scheduledProcedureStepStartDateTime=dateTime;

    responseIdentifiers->findAndGetOFString(DcmTagKey(0x0040,0x0006),ofStr,0,true);//ScheduledPerformingPhysicianName
    item.scheduledPerformingPhysicianName=ofStr.c_str();

    m_bufferDataUids<<item.studyInstanceUID;
    m_bufferData.push_back(item);
}
