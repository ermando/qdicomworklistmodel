#pragma once

#include "QDicomWorklistModel_global.h"

#include <QObject>
#include <QAbstractTableModel>
#include <QDate>
#include <QSet>

class WorklistItem{
public:
    QString accessionNumber;
    QString patientName;
    QString patientId;
    QDate patientBirthDate;
    QChar patientSex;
    QString medicalAlerts;
    QString allergies;
    QString studyInstanceUID;
    QString requestingPhysician;
    QString requestedProcedureDescription;

    //ScheduledProcedureStepSequence
    QString modality;
    QString scheduledStationAETitle;
    QDateTime scheduledProcedureStepStartDateTime;//ScheduledProcedureStepStartDate + ScheduledProcedureStepStartTime
    QString scheduledPerformingPhysicianName;

};

class DcmFindSCU;
class WorklistCallback;
class DcmDataset;
struct T_DIMSE_C_FindRQ;
struct T_DIMSE_C_FindRSP;

class QDICOMWORKLISTMODEL_EXPORT QDicomWorklistModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(QString peer READ peer WRITE setPeer NOTIFY peerChanged)
    Q_PROPERTY(unsigned int port READ port WRITE setPort NOTIFY portChanged)
    Q_PROPERTY(QString callingAet READ callingAet WRITE setCallingAet NOTIFY callingAetChanged)
    Q_PROPERTY(QString calledAet READ calledAet WRITE setCalledAet NOTIFY calledAetChanged)
public:
    enum {AccessionNumberRole=Qt::UserRole+1,PatientNameRole,PatientIdRole,PatientBirthDateRole,PatientSexRole,
          MedicalAlertsRole,AllergiesRole,StudyInstanceUIDRole,RequestedProcedureDescriptionRole,RequestingPhysicianRole,
          ModalityRole,ScheduledStationAETitleRole,ScheduledProcedureStepStartDateTimeRole,ScheduledPerformingPhysicianNameRole,LastRole};
    enum {AccessionNumberCol=0,PatientNameCol,PatientIdCol,PatientBirthDateCol,PatientSexCol,
          MedicalAlertsCol,AllergiesRoleCol,StudyInstanceUIDCol,RequestedProcedureDescriptionCol,RequestingPhysicianCol,
          ModalityCol,ScheduledStationAETitleCol,ScheduledProcedureStepStartDateTimeCol,ScheduledPerformingPhysicianNameCol,LastCol};
    explicit QDicomWorklistModel(QObject *parent = nullptr);
    ~QDicomWorklistModel() override;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    virtual QVariant data(const QModelIndex &idx, int role = Qt::DisplayRole) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

    QString peer(){return m_peer;}
    unsigned int port(){return m_port;}
    QString callingAet(){return m_callingAet;}
    QString calledAet(){return m_callingAet;}

public slots:
    void setPeer(QString peer);
    void setPort(unsigned int port);
    void setCallingAet(QString callingAet);
    void setCalledAet(QString calledAet);
    bool query();

private:
    void updateData();
    void callbackDataReady(T_DIMSE_C_FindRQ *request,int &responseCount,T_DIMSE_C_FindRSP *rsp,DcmDataset *responseIdentifiers);
    QList<WorklistItem> m_data;
    QList<WorklistItem> m_bufferData;
    QHash<int, QByteArray> m_roleNames;
    QSet <QString> m_dataUids;
    QSet <QString> m_bufferDataUids;

    QString m_peer;
    unsigned int m_port;
    QString m_callingAet;
    QString m_calledAet;
    friend class WorklistCallback;
signals:
    void peerChanged();
    void portChanged();
    void callingAetChanged();
    void calledAetChanged();

};
