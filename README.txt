-Run dcmtk_worklist_server/startserver.bat to start a dcmtk test server
-Open example project with QtCreator (qml_example/qml_example.pro or widget_example/widget_example) 
-Build and run

library is precompiled with visual studio 2019 and statically linked to dcmtk (no need of dcmtk libs to use it)
lib/QDicomWorklistModel.lib
lib/QDicomWorklistModeld.lib
bin/QDicomWorklistModel.dll
bin/QDicomWorklistModeld.dll

to compile the library set the correct path to dcmtk libs in /QDicomWorklistModel/dcmtk.pri
DCMTK_INCLUDE_DIR = ...
DCMTK_LIB_DIR = ...
