CONFIG(debug, debug|release){
    conf_dir="debug"
}
else{
    conf_dir="release"
}

DCMTK_INCLUDE_DIR = $$PWD/$$conf_dir/include
DCMTK_LIB_DIR = $$PWD/$$conf_dir/lib

INCLUDEPATH += $$DCMTK_INCLUDE_DIR

LIBS += -L$$DCMTK_LIB_DIR
LIBS += -ldcmwlm
LIBS += -ldcmsr
LIBS += -ldcmtls
LIBS += -ldcmqrdb
LIBS += -ldcmnet
LIBS += -ldcmjpeg
LIBS += -ldcmimage
LIBS += -ldcmimgle
LIBS += -ldcmdata
LIBS += -loflog
LIBS += -lofstd

LIBS += -ladvapi32
LIBS += -liphlpapi
LIBS += -lws2_32
LIBS += -lnetapi32
LIBS += -lwsock32
