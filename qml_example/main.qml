import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.12
import DICOM 1.0
ApplicationWindow {
    id: window
    width: 1024
    height: 480
    visible: true
    title: qsTr("QML example")
    DicomWorklistModel{
        id:worklistModel
        callingAet:aetField.text
        calledAet: calledAetField.text
        peer: peerField.text
        port: parseInt(portField.text)
    }

    header: //ToolBar {
        RowLayout {
            //anchors.fill: parent
            Label{text:"aet:"}
            TextField{
                id:aetField
                text:"anonimous"
            }
            Label{text:"calledAet:"}
            TextField{
                id:calledAetField
                text:"OFFIS"
            }
            Label{text:"peer:"}
            TextField{
                id:peerField
                text:"www.dicomserver.co.uk"
            }
            Label{text:"port:"}
            TextField{
                id:portField
                inputMask: "9999"
                text:"104"
            }
            Item {
                id: spacer
                Layout.fillWidth: true
            }
            ToolButton {
                id: button
                text: qsTr("Query")
                onClicked: worklistModel.query()
            }
        //}

    }
    Component{
        id:worklistItem
        ItemDelegate{
            contentItem: GridLayout{
                columns: 2
                Label{text: "accessionNumber"} Label{text: accessionNumber}
                Label{text: "patientName"} Label{text: patientName}
                Label{text: "patientId"} Label{text: patientId}
                Label{text: "patientBirthDate"} Label{text: patientBirthDate}
                Label{text: "patientSex"} Label{text: patientSex}
                Label{text: "medicalAlerts"} Label{text: medicalAlerts}
                Label{text: "allergies"} Label{text: allergies}
                Label{text: "studyInstanceUID"} Label{text: studyInstanceUID}
                Label{text: "requestingPhysician"} Label{text: requestingPhysician}
                Label{text: "requestedProcedureDescription"} Label{text: requestedProcedureDescription}

                Label{text: "modality"} Label{text: modality}
                Label{text: "scheduledStationAETitle"} Label{text: scheduledStationAETitle}
                Label{text: "scheduledProcedureStepStartDateTime"} Label{text: scheduledProcedureStepStartDateTime}
                Label{text: "scheduledPerformingPhysicianName"} Label{text: scheduledPerformingPhysicianName}
            }
        }
    }
    ListView{
        anchors.fill: parent
        clip: true
        model: worklistModel
        delegate: worklistItem
    }
}
