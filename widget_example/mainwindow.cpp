#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableView->setModel(&m_worklistModel);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_queryButton_clicked()
{
    m_worklistModel.setPeer(ui->peerEdit->text());
    m_worklistModel.setPort(ui->portSpin->value());
    m_worklistModel.setCallingAet(ui->aetEdit->text());
    m_worklistModel.setCalledAet(ui->calledAetEdit->text());
    m_worklistModel.query();
}
