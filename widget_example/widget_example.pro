QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

include(../lib/QDicomWorklistModel.pri)

CONFIG(debug, debug|release){
    TARGET = widget_exampled
}
else{
    TARGET = widget_example
}
DESTDIR = $$PWD/../bin

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui
